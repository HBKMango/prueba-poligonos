package app.android.poligonos

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import app.android.poligonos.Api.ApiData
import app.android.poligonos.Api.ApiLocation
import app.android.poligonos.Data.DataCode
import app.android.poligonos.Data.DataLocation
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {

    val apiData = ApiData()
    val apiLocation = ApiLocation()

    lateinit var fragment: SupportMapFragment
    private var map: GoogleMap? = null

    var textWatcher : TextWatcher? = null

    var data1 : String? = ""
    var data2 : List<List<Double?>> = listOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        fragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
    }

    override fun onStart() {
        super.onStart()
        fragment.getMapAsync{
            map = it
        }

        ETCode.addTextChangedListener(calculate())
    }

    private fun getDataCode(Code : String){
        apiData.api.getData(Code).enqueue(object : Callback<DataCode> {
            override fun onFailure(call: Call<DataCode>, t: Throwable) {
                Toast.makeText(applicationContext, "Error en la conexion", Toast.LENGTH_SHORT).show()
            }

            override fun onResponse(call: Call<DataCode>, response: Response<DataCode>) {
                try{
                    val body = response.body()
                    val data = body!!

                    val settlements : MutableList<String?> = mutableListOf()

                    data.settlements.forEach {
                        settlements.add(it.name)
                    }

                    ETcountry.setText("México")
                    ETfederal.setText(data.federal_entity.name)
                    ETlocality.setText(data.locality)
                    ETmun.setText(data.federal_entity.name)
                    ETsettlements.adapter = ArrayAdapter<String>(applicationContext, android.R.layout.simple_list_item_1, settlements)

                }catch (e : Exception){
                }
            }
        })
    }

    private fun getLocationsCode(Code : String){
        apiLocation.api.getLocations(Code).enqueue(object : Callback<DataLocation> {
            override fun onFailure(call: Call<DataLocation>, t: Throwable) {
                Toast.makeText(applicationContext, "Error en la conexion", Toast.LENGTH_SHORT).show()
            }

            override fun onResponse(call: Call<DataLocation>, response: Response<DataLocation>) {

                when(response.code()){
                    200 ->{
                        if (response.isSuccessful){
                            try{
                                val body = response.body()
                                val locations = body!!
                                val loc : MutableList<LatLng> = mutableListOf()

                                data2 = locations.geometry.coordinates[0]

                                data2.forEach {
                                    loc.add(LatLng(it[1]!!, it[0]!!))

                                    val bitmap = drawMarker()
                                    val sydney = LatLng(it[1]!!, it[0]!!)
                                    map!!.addMarker(MarkerOptions()
                                        .position(sydney)
                                        .icon(BitmapDescriptorFactory
                                            .fromBitmap(Bitmap.createBitmap(bitmap!!))))
                                }

                                map!!.addPolyline(
                                    PolylineOptions()
                                        .clickable(true)
                                        .addAll(loc))

                                val zoomLevel = 15.0f
                                map!!.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(loc[0].latitude, loc[0].longitude), zoomLevel))

                            }catch (e : Exception){
                            }
                        }
                    }
                    500 ->{
                        Toast.makeText(applicationContext, "Ubicaciones no disponibles", Toast.LENGTH_SHORT).show()
                        map!!.clear()
                    }
                }
            }
        })
    }

    private fun calculate() : TextWatcher?{
        if (textWatcher == null) {
            textWatcher = object : TextWatcher {

                override fun afterTextChanged(s: Editable?) {
                    if (s!!.isNotEmpty()){
                        if (s!!.length == 5){
                            //getLocationsCode("01030")
                            getDataCode(s!!.toString())
                            getLocationsCode(s!!.toString())
                            ETCode.hideKeyboard()
                        }
                    }
                }
                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
            }
        }
        return textWatcher
    }

    fun View.hideKeyboard() {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(windowToken, 0)
    }

    fun drawMarker(): Bitmap?{
        //var bitmap = Ion.with(applicationContext!!).load("https://www.onlygfx.com/wp-content/uploads/2016/09/watercolor-circle-blue-298x300.png").asBitmap().get()
        var bitmap = BitmapFactory.decodeResource(resources, R.drawable.circle)
        bitmap = Bitmap.createScaledBitmap(bitmap, 40, 40, false)
        bitmap = GoogleMapsUtils.getCroppedMarkerFromBitmap(bitmap)

        return bitmap
    }
}