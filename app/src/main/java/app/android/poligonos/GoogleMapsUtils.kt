package app.android.poligonos

import android.graphics.*
import android.location.Location
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import android.graphics.Bitmap


class GoogleMapsUtils{
    companion object {
        fun getCroppedMarkerFromBitmap(bitmap: Bitmap): Bitmap {
            val output = Bitmap.createBitmap(bitmap.width,
                    bitmap.height, Bitmap.Config.ARGB_8888)
            val canvas = Canvas(output)

            val color = -0xbdbdbe
            val paint = Paint()
            val rect = Rect(0, 0, bitmap.width, bitmap.height)

            paint.isAntiAlias = true
            canvas.drawARGB(0, 0, 0, 0)
            paint.color = color
            canvas.drawCircle((bitmap.width / 2).toFloat(), (bitmap.height / 2).toFloat(),
                    (bitmap.width / 2).toFloat(), paint)
            paint.xfermode = PorterDuffXfermode(PorterDuff.Mode.SRC_IN)
            canvas.drawBitmap(bitmap, rect, rect, paint)

            return output
        }

        private fun overlayBitmaps(bitmap1: Bitmap?, bitmap2: Bitmap?): Bitmap? {

            var finalBitmap: Bitmap? = null
            val bitmap1Width = bitmap1?.getWidth()
            val bitmap1Height = bitmap1?.getHeight()
            val bitmap2Width = bitmap2?.getWidth()
            val bitmap2Height = bitmap2?.getHeight()

            if(bitmap1Width != null && bitmap1Height != null && bitmap2Width != null && bitmap2Height != null){
                //val marginLeft = (bitmap1Width * 0.5 - bitmap2Width * 0.5).toFloat()
                //val marginTop = (bitmap1Height * 0.5 - bitmap2Height * 0.5).toFloat()

                var canvasWidth: Int? = null
                if(bitmap1Width > bitmap2Width)
                    canvasWidth = bitmap1Width
                else
                    canvasWidth = bitmap2Width

                finalBitmap = Bitmap.createBitmap(canvasWidth, bitmap1Height + bitmap2Height, bitmap1.getConfig())
                val canvas = Canvas(finalBitmap)
                canvas.drawBitmap(bitmap1, ((bitmap2Width / 2) - (bitmap1Width/2)).toFloat(), 50.toFloat(), null)
                canvas.drawBitmap(bitmap2, 0.toFloat(), 0.toFloat(), null)
                //canvas.drawBitmap(bitmap2, marginLeft, marginTop, null)
            }
            return finalBitmap
        }

        fun distanceInMeters(origin: LatLng, destiny: LatLng, originAltitude: Double, destinyAltitude:Double): Double {

            val R = 6371 // Radius of the earth

            val latDistance = Math.toRadians(destiny.latitude - origin.latitude)
            val lonDistance = Math.toRadians(destiny.longitude - origin.longitude)
            val a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + (Math.cos(Math.toRadians(origin.latitude)) * Math.cos(Math.toRadians(destiny.latitude))
                    * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2))
            val c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))
            var distance = R.toDouble() * c * 1000.0 // convert to meters

            val height = originAltitude - destinyAltitude

            distance = Math.pow(distance, 2.0) + Math.pow(height, 2.0)

            return Math.sqrt(distance)
        }

        fun getCurrentHeightMap(map:GoogleMap?): Double{
            var mapLatLngBounds = map!!.projection.visibleRegion.latLngBounds
            var southEast = LatLng(mapLatLngBounds.southwest.latitude, mapLatLngBounds.northeast.longitude)
            return distanceInMeters(southEast, mapLatLngBounds.northeast, 0.0 , 0.0)
        }

        fun getLocationFromLatLng(latLng: LatLng): Location{
            var location = Location("")
            location.latitude = latLng.latitude
            location.longitude = latLng.longitude
            return location
        }

        fun getLatLngFromLocation(location:Location):LatLng{
            return LatLng(location.latitude, location.longitude)
        }
    }
}