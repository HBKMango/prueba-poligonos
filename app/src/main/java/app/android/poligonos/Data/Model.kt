package app.android.poligonos.Data

import com.google.gson.annotations.SerializedName


//TODO---------------Data-------------------
data class DataCode(
    @SerializedName("zip_code")
    val zip_code : Int?,
    @SerializedName("locality")
    val locality  : String?,
    @SerializedName("federal_entity")
    val federal_entity : FederalEntity,
    @SerializedName("settlements")
    val settlements : List<Settlement>
)

data class FederalEntity(
    @SerializedName("key")
    val key: Int?,
    @SerializedName("name")
    val name: String?,
    @SerializedName("code")
    val code: String?
)

data class Settlement(
    @SerializedName("key")
    val key: Int?,
    @SerializedName("name")
    val name: String?,
    @SerializedName("zone_type")
    val zone_type: String?,
    @SerializedName("settlement_type")
    var settlement_type: SettlementType
)

data class SettlementType(
    @SerializedName("key")
    val key: Int?,
    @SerializedName("name")
    val name: String?
)

//TODO-------------Location------------------
data class DataLocation(
    @SerializedName("type")
    val type : String?,
    @SerializedName("geometry")
    val geometry : Geometry
)

data class Geometry(
    @SerializedName("type")
    val type : String?,
    @SerializedName("coordinates")
    val coordinates : List<List<List<Double?>>>
)