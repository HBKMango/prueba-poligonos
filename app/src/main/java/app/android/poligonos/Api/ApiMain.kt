package app.android.poligonos.Api

import android.content.Context
import com.google.gson.Gson
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ApiData() {

    companion object {
        val BASE_URL: String="https://sepomex-wje6f4jeia-uc.a.run.app/"
        val URL: String = BASE_URL + "api/zip-codes/"
    }
    val api: ApiEndPoints

    init {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        val clientBuilder = OkHttpClient.Builder()
        clientBuilder.addInterceptor(logging)
        val client = clientBuilder.build()
        val retrofit = Retrofit.Builder()
            .baseUrl(URL)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        api = retrofit.create(ApiEndPoints::class.java)
    }
}

class ApiLocation() {

    companion object {
        val BASE_URL: String="https://poligonos-wje6f4jeia-uc.a.run.app/"
        val URL: String = BASE_URL + "zip-codes/"
    }
    val api: ApiEndPoints

    init {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        val clientBuilder = OkHttpClient.Builder()
        clientBuilder.addInterceptor(logging)
        val client = clientBuilder.build()
        val retrofit = Retrofit.Builder()
            .baseUrl(URL)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        api = retrofit.create(ApiEndPoints::class.java)
    }
}

object ErrorParser {
    fun parseError(error: String?): String{
        error?.let {
            var message = ""
            try {
                val obj = Gson().toJsonTree(error).asJsonObject
                obj.entrySet().map {
                    message = message + " | " + it.value.asJsonArray[0]
                }
            } catch (e: Exception){
                e.printStackTrace()
            }
            return message
        }
        return ""
    }
}

interface ErrorHandler{
    fun OnError(error:String)
}