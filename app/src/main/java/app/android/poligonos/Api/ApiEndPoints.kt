package app.android.poligonos.Api

import app.android.poligonos.Data.DataCode
import app.android.poligonos.Data.DataLocation
import retrofit2.Call
import retrofit2.http.*

interface ApiEndPoints{

    //@GET("zip-codes/")
    //fun getData(@Query("") Code: String): Call<DataCode>

    @GET("{code}")
    fun getData(@Path("code") code: String): Call<DataCode>

    @GET("{code}")
    fun getLocations(@Path("code") code: String): Call<DataLocation>

}